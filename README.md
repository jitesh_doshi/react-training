# ReactJS Training #

This is a sample project that teaches ReactJS programming step-by-step. We start from scratch
(a few small, simple code-base), and then advance toward more functionality and complexity.

## How to use this repository? ##

* Clone this repository with `git clone https://bitbucket.org/jitesh_doshi/react-training`
* View the various tags. They are named like `step_1`, `step_2` etc.
* Checkout the code using their tags, such as `git checkout step_1`.
* View `README.md` (this file) for instructions on how to get to next step.

# Overview #

[ReactJS](https://facebook.github.io/react/) is a framework for creating JavaScript
applications organized as components. Please visit [ReactJS](https://facebook.github.io/react/)
website for details. You will need to install NodeJS and NPM.

# Instructions: step 2 #
1. Install NodeJS and NPM from [https://nodejs.org/en/download/]
2. In the base directory of this project, run `npm init` and take all the defaults by pressing `ENTER` for all questions.
3. The step above creates `package.json` file. View it and understand the structure.
4. Run `npm install`. This creates a new `node_modules` directory in your project base. Initially the directory will be empty because there are no dependencies to download. But in future, this will contain NodeJS modules that your project depends upon.
5. Create a `.gitignore` file containing the following. This will tell git not to worry about the node_modules directory.
> /node_modules

# Instructions: step 3 - run some JavaScript code #
1. Create a new folder named `src` in your project base.
2. Create a new file named `server.js` containing the following code.
```
console.log('This is where the server will be.');
```
3. Run it as `node src/server.js`. It should produce the expected message.

# Instructions: step 4 - adding npm scripts #
1. Add the following to `package.json` under `scripts` before the "test" entry.
> "start": "node src/server.js",
2. Execute the above script with `npm start`

With this, we are able to run arbitrary commands from npm.

# Instructions: step 5 - running a web server #
1. Let's add `express` (a NodeJS web server) as a dependency by running `npm install express --save`
2. Edit `server.js` file and change the contents to the following ...
```
// load express
var express = require('express');
// create express object
var app = express();
const port=3000;

// add route for ALL (*) paths to serve index.html
app.get('*', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});

// start server listening on said port
app.listen(port, function(err) {
  // handle success or failure
  if(err) {
    console.log(err);
  } else {
    console.log('Server is now running. Visit http://localhost:3000/ in your browser.');
  }
});
```
3. Create a new file src/index.html with following contents.
```
<!DOCTYPE html>
<html>
  <head>
    <title>ReactJS Training</title>
  </head>
  <body>
    <h1>ReactJS Training</h1>
  </body>
</html>
```
4. Re-run `npm start` and then visit http://localhost:3000/ in the browser.

# Instructions: step 6 - Trying to use ES6 "import" (and failing) #
1. Replace the first code line of server.js from "require" to "import" like this ...
```
import express from 'express';
```
2. Try to run `npm start`. You will see that it fails.

The reason why the above fails is because "import" is an ES6 feature and node, by default, cannot run ES6.

# Instructions: step 7 - Transpiling ES6 using Babel #
In order to run ES6 on the node, a transpiler like `Babel` is required to convert to ES5 or CommonJS. 

Let's add `babel-cli`,`babel-core`, `babel-preset-es2015`  as a dependency by running 
```sh
npm install babel-cli babel-core babel-preset-es2015 --save-dev
```

Update the `start` script in `package.json` to the following.
```sh
"start": "babel-node --presets babel-preset-es2015 src/server.js"
```

Here `babel-node` compiles ES6 code before running it using the presets specified.
Now run `npm start` and then visit http://localhost:3000/ in the browser.

# Instructions: step 8 - Improving Web Server #
Add the following npm modules `chalk` and `open` to add terminal string styling and to open the specified url `http://localhost:3000/`
on `npm start`.

```sh
npm install chalk open --save-dev
```

Update the `server.js` file with the following.
```
import express from 'express';
import open from 'open';
import chalk from 'chalk';

// create express object
const app = express();
const port=3000;

// add route for ALL (*) paths to serve index.html
app.get('*', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});

// start server listening on said port
app.listen(port, function(err) {
  // handle success or failure
  if(err) {
    console.log(err);
  } else {
    console.log(chalk.green('Server is now running. Visit http://localhost:3000/ in your browser.'));
    open(`http://localhost:${port}`);
  }
});
```

Try to run `npm start`. You will see sever running and opening the url `http://localhost:3000/` for you.



