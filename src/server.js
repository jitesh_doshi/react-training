// import express
import express from 'express';
// create express object
var app = express();
const port=3000;

// add route for ALL (*) paths to serve index.html
app.get('*', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});

// start server listening on said port
app.listen(port, function(err) {
  // handle success or failure
  if(err) {
    console.log(err);
  } else {
    console.log('Server is now running. Visit http://localhost:3000/ in your browser.');
  }
});
